#coding:utf-8
import os
import datetime
import apsw
import threading

class BaseDb(object):


    def __init__(self,db_path):

        ''' 
            数据库连接初始化
            sqlite默认时不支持多线程的,用apsw库支持多线程
        '''
        self.conn = apsw.Connection(db_path)
        self.cursor = self.conn.cursor()
        #线程安全锁
        self._lock = threading.Lock()

    def close(self):

        try:
            self.cursor.close()
        finally:
            self.conn.close()

    def create_table(self,sql,table_name):
        
        '''
            创建数据库表
        '''
        self.cursor.execute(sql)
#        self.conn.commit()

    def save(self,sql, datas):
        
        '''
            插入数据
        '''
        with self._lock:
            if datas is not None:
                for data in datas:
                    data = self.convert_to_data(data)
                    self.cursor.execute(sql, data)
 #               self.conn.commit()

    def fetchall(self,sql):
        
        '''
            查询所有数据
        '''
        self.cursor.execute(sql)
        results = self.cursor.fetchall()
        return results

    def fetchone(self,sql, data=None):
        
        '''
            查询一条数据
        '''
        with self._lock:
            if data is not None:
                self.cursor.execute(sql, data)
                result = self.cursor.fetchone()
            else:
                self.cursor.execute(sql)
                result = self.cursor.fetchone()
            return result

    def update(self,sql, datas=None):
        
        '''
            更新数据
        '''
        
        with self._lock:
            if datas is not None:
                for data in datas:
                    data = self.convert_to_data(data)
                    self.cursor.execute(sql, data)
        #            self.conn.commit()
            else:
                self.cursor.execute(sql)
        #        self.conn.commit()

    def delete(self,sql, datas=None):
        
        '''
            删除数据
        '''
        if datas is not None:
            for data in datas:
                self.cursor.execute(sql, data)
     #           self.conn.commit()
        else:
            self.cursor.execute(sql)
     #       self.conn.commit()

    def convert_to_data(self,data):
        assert(type(data) == tuple)
        lst = list(data)
        for i,v in enumerate(lst):
            if type(v) == datetime.datetime:
                lst[i] = str(v)

        return tuple(lst)
