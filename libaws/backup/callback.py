import threading
import time
import os
import sys

class ProgressPercentage(object):

    def __init__(self, filename,seen_so_far=0): 
        self._filename = filename
        self._size = 0.0 
        self._seen_so_far = seen_so_far 
        self._lock = threading.Lock()
        self.start_time = time.time() 
        self.first = True
        self.SPEED_TIME_SEC = 1 
        self.time_secs = 0.0 
        self.time_bytes = 0.0
        self.sec_speed = 0.0

    def __call__(self, bytes_amount):
                # To simplify we'll assume this is hooked up
        # to a single filename.
        with self._lock:
            self._seen_so_far += bytes_amount
            percentage = (self._seen_so_far / float(self._size)) * 100

            if percentage > 100:
                print self._filename,'upload finished'
                return
            
            
            now_time = time.time()
            if self.first:
                time_sec = now_time - self.start_time
                self.first = False
            else:
                time_sec = now_time - self.last_time
            
            self.time_secs += time_sec
            self.time_bytes += bytes_amount

            if self.time_secs >= self.SPEED_TIME_SEC:
                self.sec_speed = (self.time_bytes/1024.0)/self.time_secs
            #    print 'time sec:',self.time_secs,'bytes_count:',self.time_bytes
                self.time_secs = 0.0
                self.time_bytes = 0.0
         
            sys.stdout.write("\r%s  %s / %s  (%.2f%%),speed:%10.3f KB/S" % (self._filename, self._seen_so_far,\
                                self._size, percentage,self.sec_speed))

            if percentage == 100:
                sys.stdout.write('\n')
            sys.stdout.flush()
            self.last_time = time.time()


class DownloadProgressPercentage(ProgressPercentage):

    def __init__(self, filename,size,seen_so_far=0): 
        super(DownloadProgressPercentage,self).__init__(filename,seen_so_far)
        self._size = size

class UploadProgressPercentage(ProgressPercentage):

    def __init__(self, filename,seen_so_far=0): 
        super(UploadProgressPercentage,self).__init__(filename,seen_so_far)
        self._size = os.path.getsize(filename) 
